require 'byebug'
class Student
  attr_reader :first_name, :last_name
  attr_accessor :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    unless course.students.include?(self)
      course_conflict = course_conflict(course)
      raise_error(course.name, course_conflict.name) if course_conflict
      courses << course
      course.students << self
    end
  end

  def course_load
    course_load = Hash.new(0)
    courses.each { |course| course_load[course.department] += course.credits }
    course_load
  end

  def course_conflict(other_course)
    courses.each do |course|
      return course if other_course.conflicts_with?(course)
    end
    nil
  end

  def raise_error(course_name, other_course_name)
    raise "#{course_name} conflicts with #{other_course_name}"
  end

  # def ==(other_student)
  #   self.first_name == other_student.first_name &&
  #   self.last_name == other_student.last_name &&
  #   self.courses == other_student.courses
  # end
end
